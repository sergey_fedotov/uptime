#pragma once
#include <Arduino.h>

namespace UptimeType {
	struct UptimeT {
		unsigned long day = 0;
		uint8_t hour = 0;
		uint8_t minute = 0;
		uint8_t second = 0;		
	};	
};

class Uptime {
private:
	static UptimeType::UptimeT time;
	static bool highMillis;
	static int rollover;
	static const unsigned long highMillisStart PROGMEM = 3000000000UL;
	static const unsigned long rolloverStart PROGMEM = 100000UL;
	char * buf_ = nullptr;

public:

	Uptime(){
		get();
	 };
	~Uptime(){
		if ( buf_ != nullptr){
			delete buf_;
		}
	};
	UptimeType::UptimeT get(){
		unsigned long now = millis();
		if ( now >= highMillisStart ){
			highMillis = true;	
		}
		if ( now <= rolloverStart && highMillis ){
			rollover++;
			highMillis = !highMillis;
		} 
		unsigned long secsUp = now/1000;
		uint second = secsUp%60;
		uint minute = (secsUp/60)%60;
		uint hour = (secsUp/(60*60))%24;
		unsigned long day = (rollover*50)+(secsUp/(60*60*24));

		time = { day: day, hour:(uint8_t)hour, minute:(uint8_t)minute, second:(uint8_t)second};
		return time;
	};
	inline void polling(){ 
		get(); 
	};
	inline unsigned long Days(){ return time.day; };
	inline uint8_t Hours(){ return time.hour; };
	inline uint8_t Minutes(){ return time.minute; };
	inline uint8_t Seconds(){ return time.second; };
	const char * toString(bool outSecunds = false) const;
	const char * toString(const char *, ...);
};

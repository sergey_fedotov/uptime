#include <uptime.h>

Uptime uptime;

void setup(){
	Serial.begin(9600);
}

void loop(){	

	UptimeType::UptimeT up = uptime.get();
	Serial.printf("Uptime %d days %2.2d h %2.2d m %2.2d s\n", 
		(int)up.day, 
		up.hour, 
		up.minute, 
		up.second);
   delay(1000);
   Serial.println( uptime.toString("Uptime %lu days %2.2d:%2.2d:%2.2d") );

   if ( millis() > 10000){
      Uptime newUptime;

      Serial.printf("Uptime: %s\n", newUptime.toString());
   }
}

#include <uptime.h>


int Uptime::rollover = 0;
UptimeType::UptimeT Uptime::time ={};
bool Uptime::highMillis = false;

/*
UptimeType::UptimeT Uptime::get(){
	unsigned long now = millis();
	if ( now >= highMillisStart ){
		highMillis = true;	
	}
	if ( now <= rolloverStart && highMillis ){
		rollover++;
		highMillis = !highMillis;
	} 
	unsigned long secsUp = now/1000;
	uint second = secsUp%60;
	uint minute = (secsUp/60)%60;
	uint hour = (secsUp/(60*60))%24;
	unsigned long day = (rollover*50)+(secsUp/(60*60*24));

	time = { day: day, hour:(uint8_t)hour, minute:(uint8_t)minute, second:(uint8_t)second};
	return time;
};
*/

const char * Uptime::toString(bool outSeconds ) const {
	static char buf[35] = {0};
	int pointer = 0;	
	if ( time.day > 0 ){
		pointer += sprintf(buf, (const char*)F("%lu days "), time.day );
	}
	if ( time.hour > 0 ){
		pointer += sprintf(&buf[pointer], (const char*)F("%d hours "), time.hour );	
	}
	if ( time.minute > 0 ){
		pointer += sprintf(&buf[pointer], (const char*)F("%d mins "), time.minute );	
	}
	if ( pointer == 0 || ( outSeconds && time.second > 0 )){
		pointer += sprintf(&buf[pointer], (const char*)F("%d secs "), time.second );	
	}
	buf[pointer] = '\0';
	return buf;
};

const char * Uptime::toString(const char * format, ...) {
	if ( buf_ == nullptr ){
		buf_ = (char *)malloc( (char)(strlen(format) +10 ) );
	}
	va_list args;
	va_start(args, format);
	int pointer = vsnprintf(buf_, strlen(format) +10, format, args);
	va_end(args);
	//sprintf(buf_, format, time.day, time.hour, time.minute, time.second);
	buf_[pointer] = '\0';
	return buf_;
};
